package com.example.praktika;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class User {
    private String _login;
    private String _password;
    private String _site;

    public User(String name, String age)
    {
       _login = name;
        _password = age;
    }

    public User(){}

    public String getLogin(){
        return _login;
    }
    public String getPassword(){ return _password; }
    public void setLogin(String login){
        _login = login;
    }
    public void setPassword(String password){
        _password = password;
    }
    public String getSite() { return _site; }
    public void setSite(String site) {_site = site; }

    @NotNull
    public String toString(){
        return  "User: " + _login + " - " + _password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(_login, user._login) &&
                Objects.equals(_password, user._password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_login, _password);
    }

}