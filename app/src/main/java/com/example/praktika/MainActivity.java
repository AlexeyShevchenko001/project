package com.example.praktika;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.xmlpull.v1.XmlPullParser;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    Button button;
    EditText login;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.enter);
        login= (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);


        XmlPullParser xpp = getResources().getXml(R.xml.user);
        UserResourceParser parser = new UserResourceParser();

        if(parser.parse(xpp))
        {
            for(User prod: parser.getUsers()){
                Log.d("XML", prod.toString());
            }
        }

        final ArrayList<User> Users = parser.getUsers();

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                String tempLogin = login.getText().toString().trim();
                String tempPassword = password.getText().toString().trim();

                if (tempLogin.equalsIgnoreCase("")) {
                    login.setError(getResources().getString(R.string.empty_name));
                    return;
                }
                else if (tempPassword .equalsIgnoreCase("")) {
                    password.setError(getResources().getString(R.string.empty_name));
                    return;
                }

                User user = new User(tempLogin, tempPassword);

                if(Users.contains(user))
                {
                    user = Users.get(Users.indexOf(user));

                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("Login", user.getLogin());
                    intent.putExtra("Site", user.getSite());
                    startActivity(intent);
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Ошибка ввода!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

    }
}